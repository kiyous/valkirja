package net.daemonyum.valkirja;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.daemonyum.valkirja.product.Item;

/**
 * 
 * @author Kevin VANDAMME
 *
 */
public class Category implements Comparable<Category>{

	private Date creationDate;
	private List<Item> items;

	private CategoryLevel level;//(UNIVERSE,FAMILY,SUBFAMILY)
	private String name;

	private List<Category> subcategories;
	private Date updateDate;

	private boolean visible;
    private Date beginningDateActive;
    private Date endDateActive;


	public void addCategory(Category newCat) {
		subcategories.add(newCat);
		Collections.sort(subcategories);
	}

	//    public void addCategories(Category ... newCats) {
	//        subcategories.addAll(newCats);
	//        Collections.sort(subcategories);
	//    } 

	public void addItem(Item newIt) {
		items.add(newIt);
		Collections.sort(items);
	}

	//    public void addItems(Item ... newIts) {
	//        items.addAll(newIts);
	//        Collections.sort(items);
	//    }

	@Override
	public int compareTo(Category c) {
		return name.compareTo(c.name);
	}

}
