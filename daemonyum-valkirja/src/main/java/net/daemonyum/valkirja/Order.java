package net.daemonyum.valkirja;

import java.util.Date;
import java.util.List;

import net.daemonyum.valkirja.discount.Discount;

/**
 * 
 * @author Kevin VANDAMME
 *
 */
public class Order {
    
	private List<SelectedProduct> products;
    private List<Discount> discounts;
    
    private long total;
    
//    private ShippingService shipping;
//    private ShippingAddress;
    
    private Date creationDate;
    private Date updateDate;

}
