package net.daemonyum.valkirja;

public enum OrderStatus {
    
    WAITING,//en attente de paiement
    RECORDED,//enregistrée
    VALIDATED,//validée
    IN_PROGRESS,//en préparation
    SENT,//envoyée
    SHIPPED,//livrée
    CANCELED,//annulée
    ;
    
}