package net.daemonyum.valkirja.product;

import java.util.Date;

/**
 * Un produit
 * @author Kevin VANDAMME
 *
 */
public class Reference extends Item {

	public String color;    
	private Date creationDate;    
	private Long discountPrice;    
	private Long price;    
	private String reference;
	private Date updateDate;    
	private boolean visible;

	@Override
	public boolean isVisible() {		
        return super.isVisible() && visible;
	}

}
