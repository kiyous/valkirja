package net.daemonyum.valkirja.product;

import java.util.Date;

/**
 * Un article
 * @author Kevin VANDAMME
 *
 */
public class StockKeepingUnit extends Reference{//SKU	

	private Date creationDate;	
	private Double price;	
	private String size;    
	private Integer stock;
	private Date updateDate;    
	private boolean visible;
    private Date beginningDateActive;
    private Date endDateActive;

	@Override
	public boolean isVisible() {
		return super.isVisible() && visible;
	}

}
