package net.daemonyum.valkirja.product;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Un produit
 * @author Kevin VANDAMME
 *
 */
@Getter
@Setter
public class Item implements Comparable<Item>{

	private String brand;	
	private String code;
	private Date creationDate;
	private String description;
	private String name;
	private Date updateDate;
	private boolean visible;


	@Override
	public int compareTo(Item i) {
		return name.compareTo(i.name);
	}

}
