package net.daemonyum.valkirja;

import java.util.Date;

/**
 * 
 * @author Kevin VANDAMME
 *
 */
public class Customer {
    
    private String lastname;
    private String secondname;
    private String middlename;
    private String firstname;
    private Date birthdate;    
    private Address address;
    
    private String email;
    
    private Date creationDate;
    private Date updateDate;
    
    
    private Caddy persistentCaddy;
    
    

}
